<!DOCTYPE html>
<html ng-app="BigJApp" ng-controller="BaseController">

<head>

  <meta charset="UTF-8">
  <title></title>
  <asset:stylesheet href="application.css" />
  <asset:javascript src="application.js" />

</head>

<body>

  <div class="navbar navbar-fixed-top nav-color" style="margin-bottom: 1em" ng-if="isAuthenticated()">
    <div class="container-fluid">
      <div class="navbar-header">
        <img class="imgtop" src="../BigJRepository/images/bgjname9.png">
        <a href="../" class="navbar-brand app_name"></a>
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main"></button>
      </div>

      <div class="navbar-collapse collapse" id="navbar-main">
        <ul class="nav navbar-nav navbar-right">
          <li ui-sref-active="active">
            <a class="nav-text" ui-sref="home"><span class="glyphicon glyphicon-home"></span> HOME</a>
          </li>
          <!-- <li ui-sref-active="active">
            <a ui-sref="storage" class="nav-text" ng-if="isAuthenticated()">STORAGE</a>
          </li> -->
          <li>
            <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle">SUPPLIER<b class="caret"></b>
                    </a>
            <ul class="dropdown-menu">
              <!-- <li><a href="#" ui-sref="add-supplier">Add Supplier</a></li> -->
              <li><a href="#" ui-sref="suppliers">View Suppliers</a></li>
              <li><a href="#" ui-sref="add-purchase-order-transaction">Add Purchase Order</a></li>
              <li><a href="#" ui-sref="purchase-order-transactions">Purchase Orders</a></li>
            </ul>
          </li>
          <li>
            <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle">CLIENT<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <!-- <li><a href="#" ui-sref="add-client">Add Client</a></li> -->
              <li><a href="#" ui-sref="clients">View Clients</a></li>
              <li><a href="#" ui-sref="add-pricelist-transaction">Add Pricelist</a></li>
              <li><a href="#" ui-sref="pricelist-transactions">View Pricelist</a></li>

            </ul>
          </li>
          <li ui-sref-active="active">
            <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle">INVENTORY <b class="caret"></b>
                    </a>
            <ul class="dropdown-menu">
              <li><a href="" ui-sref="products">Products</a></li>
              <li><a href="" ui-sref="storage">Storage</a></li>
            </ul>
          </li>
          <li>
            <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle" ng-if="isAuthenticated()">PURCHASE ORDER <b
                            class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Aldeguer</a></li>
              <li><a href="#">Hoskyn</a></li>
              <li><a href="#">Tanza</a></li>
            </ul>
          </li>
          <li>
            <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle">DTR<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Aldeguer</a></li>
              <li><a href="#">Hoskyn</a></li>
              <li><a href="#">Tanza</a></li>
            </ul>
          </li>
          <li>
            <select data-toggle="dropdown" class="form-control" ng-model="branch" ng-change="changeBranchId(branch)">
              <option disabled value selected>Choose branch</option>
              <option value="1">ALDEGUER</option>
              <option value="2">HOSKYN</option>
              <option value="3">TANZA</option>
            </select>
          </li>

          <!-- <li><a href="#" ng-click="logout()" name="logout" ng-if="isAuthenticated()" class="nav-text"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li> -->
        </ul>
      </div>
    </div>

    <div class="header-top">
      <div class="layout-wrapper">
        <a href="#" data-toggle="dropdown" class="nav-text dropdown-toggle">
          <!-- <button class="btn btn-default">Select branch</button><b class="caret"></b></a> -->

          <ul class="pull-right">
            <!-- <select data-toggle="dropdown" class="form-control" ng-model="branch" ng-change="changeBranchId(branch)">
          <option value="1" selected>ALDEGUER</option>
          <option value="2">HOSKYN</option>
          <option value="3">TANZA</option>
        </select> -->
            <!-- <li><p class="nav-text">You are logged in:</li> -->
            <li><a href="#" ng-click="logout()" name="logout" class="nav-text"><span class="glyphicon glyphicon-log-out"></span> Logout   </a></li>
          </ul>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div ui-view></div>
    </div>
  </div>
</body>

</html>
