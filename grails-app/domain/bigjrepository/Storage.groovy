package bigjrepository

class Storage {
	Product product
	String stockNumber
	Supplier supplier
	int quantity
	Branch branch
	// int temporaryQuantity

    static constraints = {
    	product(unique: 'branch')
    	branch nullable: true
    	quantity nullable: true
    }
}
