package bigjrepository
// import org.joda.time.*

class PurchaseOrderTransaction {
	Supplier supplier
	// String status
	String shipVia
	static hasMany = [orderedProducts: PurchaseOrderProduct, notes: PurchaseOrderNote]
	Date dateCreated
	Date lastUpdated

	Date dateOrdered
	Date dateReceived

	Branch branch
	
    static constraints = {
    	dateOrdered nullable: true
    	dateReceived nullable: true
    	branch nullable: true
    }
}
