package bigjrepository

class Category {
	String name
    static hasMany = [subCategories: SubCategory]

    static constraints = {
        subCategories cascade: "all-delete-orphan"
    }
}
