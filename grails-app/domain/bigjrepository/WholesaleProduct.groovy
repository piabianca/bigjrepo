package bigjrepository

class WholesaleProduct {
	Product product
	double price
	String stockNumber
	static belongsTo = [transaction: WholesaleTransaction]
	static mappedBy = [product: "none"]

    static constraints = {
        product nullable: true
        stockNumber nullable: true
    }

}
