package bigjrepository

class Supplier {
	String name
	String contactNumber
	String address
	Date dateCreated
	Date lastUpdated

    static constraints = {
    }
}
