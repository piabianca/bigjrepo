package bigjrepository

class PurchaseOrderProduct {

    // static hasOne = [product: Product]
	Product product
	int quantity
	String unit
	String stockNumber
	String description
	boolean newProduct
	double unitPrice
	static belongsTo = [transaction: PurchaseOrderTransaction]
	static mappedBy = [product: "none"]

    static constraints = {
    	product nullable: true
    	description nullable: true
    	stockNumber nullable: true
    	newProduct nullable: true
    }
}
