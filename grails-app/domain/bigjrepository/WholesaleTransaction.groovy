package bigjrepository

class WholesaleTransaction {
    Client client
    Branch branch
    static hasMany = [wholesaleProducts: WholesaleProduct]
    Date dateCreated
    Date lastUpdated

    static constraints = {
        wholesaleProducts cascade: "all-delete-orphan"
        branch nullable: true
    }
}
