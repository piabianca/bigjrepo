angular.module('BigJApp').controller('CategoryController', function ($scope, $state, $stateParams, Category, SubCategory, Branch, branchId) {
    var categoryId = $stateParams.id;
    $scope.categories = Category.query();
    $scope.subCategories = SubCategory.query();
    $scope.selectedCategory = null;
    $scope.newCategory = null;

    if ($stateParams.id != null) {
        $scope.category = Category.get({category: categoryId});
    }
    $scope.subCategoryName = {content: null};

    $scope.setCategory = function (category) {
        $scope.selectedCategory = category;
    };

    $scope.addCategory = function () {
        var category = new Category();
        category.name = $scope.newCategory.name;
        Category.save(category, function () {
            setTimeout(function () {
                $scope.categories = Category.query();
            }, 1000);
        });
    };

    $scope.addSubCategory = function () {
        var subCategory = new SubCategory();
        subCategory.name = $scope.subCategoryName.content;
        subCategory.category = $scope.selectedCategory.id;
        SubCategory.save(subCategory, function () {
            setTimeout(function () {
                $scope.subCategories = SubCategory.query();
            }, 1000);
        });
        $scope.subCategoryName = {content: null};
    };

    $scope.deleteCategory = function (category) {
        Category.delete({category: category.id}, function () {
            setTimeout(function () {
                $scope.categories = Category.query();
            }, 1000);
        });
    };

    $scope.deleteSubCategory = function (subCategory) {
        SubCategory.delete({subCategory: subCategory.id}, function () {
            setTimeout(function () {
                $scope.subCategories = SubCategory.query();
            }, 1000);
        });
    };

    $scope.update = function () {
        var category = Category.get({category: categoryId});
        category.name = $scope.category.name;
        Category.update({category: categoryId}, category, function () {
            $state.go('categories');
        });
    };

    $scope.branches = Branch.query(function() {
        $scope.selected = $scope.branches[0].id;
    });

    $scope.onChange = function(branch) {
        // console.log(branch.id);
        branchId.value = branch.id;
        console.log("global id: " + branchId.value);
    };
});
