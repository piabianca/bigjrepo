angular.module('BigJApp').controller('UserController', function ($scope, $auth, $state, User, Branch) {
    $scope.login = function () {
        var USER = {
            username: $scope.username,
            password: $scope.password
        };
        $auth.login(USER)
            .then(function (response) {
                $state.go('products');
            });
    };

    $scope.branches = Branch.query();

    $scope.create = function () {
        var user = new User.User();
        user.username = $scope.formInfo.username;
        user.password = $scope.formInfo.password;
        user.requested_role = parseFloat($scope.formInfo.branch) + 2;
        User.User.save(user);
    };

});
