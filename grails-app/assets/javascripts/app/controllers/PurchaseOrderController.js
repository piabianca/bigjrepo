angular.module('BigJApp').controller('PurchaseOrderController', function($scope, $state, PurchaseOrder, Product, $stateParams, BP, SP, branchId, Storage) {
    if($state.current.name == "purchase-order-transactions") {
        $scope.transactions = PurchaseOrder.transaction.query();

        $scope.delete = function(transaction) {
            PurchaseOrder.transaction.delete({orderedTransaction: transaction}, function() {
                console.log("transaction deleted");
            });
        };
    }
    else {
        // $scope.print = function() {
        //     window.print();
        // }
        $scope.prods = BP.get({bp: branchId.value});
        //$scope.productss = Product.query();
        var userID = $stateParams.id;

        //TRANSACTION
        $scope.products = [];
        $scope.one = PurchaseOrder.transaction.get({orderedTransaction: userID}, function() {
            $scope.products = PurchaseOrder.transactionProduct.get({tp: userID}, function() {
                $scope.getTotal = function(){
                    var total = 0;
                    for(var i = 0; i < $scope.products.length; i++){
                        var product = $scope.products[i];
                        total += (product.unitPrice * 1);
                    }
                    return total;
                }
            });

            //X EDITABLE
            $scope.getTotal = function(){
                var total = 0;
                for(var i = 0; i < $scope.products.length; i++){
                    var product = $scope.products[i];
                    total += (product.unitPrice * 1);
                }
                return total;
            }

            $scope.saveUser = function(data, id) {
                PurchaseOrder.product.update({orderedProduct: id}, data, function() {
                    // console.log("SUCCESS!!");
                });
            };

            // remove user
            $scope.removeUser = function(index, product) {
                PurchaseOrder.product.delete({orderedProduct: product.id});
                if(product.newProduct == 1) {
                    $scope.storage = SP.get({storage: product.product}, function() {
                        Storage.delete({storage: $scope.storage.id}, function() {
                        });
                    });
                }
                $scope.products.splice(index, 1);
            };

            // add user
            $scope.addUser = function() {
                if($scope.selected == 'new'){
                    var product = new Product();
                    product.name = $scope.productName;
                    product.color = $scope.productColor;
                    product.size = $scope.productSize;
                    product.description = $scope.productDescription;
                    product.price = $scope.productPrice;
                    $scope.newProduct = Product.save(product, function() {
                        var ordered_product = new PurchaseOrder.product();
                        ordered_product.product = $scope.newProduct.id;
                        ordered_product.quantity = $scope.quantity;
                        ordered_product.unit = $scope.unit;
                        ordered_product.unitPrice = $scope.unitPrice;
                        ordered_product.transaction = $scope.one.id;
                        ordered_product.newProduct = 1;
                        $scope.op = PurchaseOrder.product.save(ordered_product, function() {
                            var storage = new Storage();
                            storage.product = $scope.newProduct.id;
                            storage.stockNumber = $scope.stockNumber;
                            //
                            // storage.quantity = $scope.quantity;
                            storage.supplier = $scope.one.supplier;
           ///need branch id
                            storage.branch = branchId.value;
                            $scope.store = Storage.save(storage, function() {
                                // console.log($scope.store);
                            });
                            $scope.pID = $scope.op.id;
                            $scope.inserted = {
                                id: $scope.pID,
                                product: $scope.newProduct.id,
                                quantity: $scope.quantity,
                                unit: $scope.unit,
                                stockNumber: $scope.stockNumber,
                                color: $scope.productColor,
                                description: $scope.description,
                                unitPrice: $scope.unitPrice,
                                newProduct: 1
                            };
                            $scope.products.push($scope.inserted);
                        });
                    });
                }
                else {
                    var ordered_product = new PurchaseOrder.product();
                    ordered_product.product = $scope.storageProduct;
                    ordered_product.quantity = $scope.quantity;
                    ordered_product.unit = $scope.unit;
                    ordered_product.unitPrice = $scope.unitPrice;
                    ordered_product.transaction = $scope.one.id;
                    $scope.op = PurchaseOrder.product.save(ordered_product, function() {

                        //product in storage
                        // var product = SP.get({storage: $scope.storageProduct}, function() {
                        //     var newQuantity = (parseInt(product.quantity) + parseInt($scope.quantity));
                        //     var newData = {quantity: newQuantity};
                        //     Storage.update({storage: product.id}, newData, function() {
                        //         console.log("updated")
                        //     });
                        // });
                        $scope.pID = $scope.op.id;
                        $scope.inserted = {
                            id: $scope.pID,
                            product: $scope.storageProduct,
                            quantity: $scope.quantity,
                            unit: $scope.unit,
                            stockNumber: $scope.storageProduct,
                            color: $scope.storageProduct,
                            description: $scope.description,
                            unitPrice: $scope.unitPrice,
                            newProduct: 0
                        };
                        $scope.products.push($scope.inserted);
                    });
                }
            };
        });
    }
});