angular.module('BigJApp').controller('WholesaleController', function($scope, $state, Wholesale, SP, BP, branchId, Product, $stateParams) {
    if($state.current.name == "pricelist-transactions") {
        $scope.transactions = Wholesale.transaction.query();
        $scope.delete = function(transaction) {
            Wholesale.transaction.delete({wholesaleTransaction: transaction}, function() {
                console.log("transaction deleted");
            });
        };
    }
    else {
        $scope.productss = BP.get({bp: branchId.value});
        var userID = $stateParams.id;
        console.log($scope.productss);
        //TRANSACTION
        $scope.products = [];
        $scope.one = Wholesale.transaction.get({wholesaleTransaction: userID}, function() {
            $scope.products = Wholesale.transactionProduct.get({tp: userID}, function() {
                // $scope.getTotal = function(){
                //     var total = 0;
                //     for(var i = 0; i < $scope.products.length; i++){
                //         var product = $scope.products[i];
                //         total += (product.price * product.quantity);
                //     }
                //     return total;
                // }
            });

             $scope.selectedItemChanged = function (product) {
                $scope.item = SP.get({storage: product}, function () {
                    $scope.itemInstance = $scope.item;
                });
            }

            //X EDITABLE
            // $scope.productss = Product.query();

            $scope.groups = [];
            $scope.loadGroups = function() {
                return $scope.groups.length ? null : Product.query(function(data) {
                    $scope.groups = data;
                });
            };

            $scope.showGroup = function(user) {
                // if(user.group && $scope.groups.length) {
                //     var selected = $filter('filter')($scope.groups, {id: user.group});
                //     return selected.length ? selected[0].text : 'Not set';
                // } else {
                return user.product || 'Not set';
                // }
            };

            $scope.saveUser = function(data, id) {
                Wholesale.product.update({wholesaleProduct: id}, data, function() {
                    console.log("SUCCESS!!");
                });
            };

            // remove user
            $scope.removeUser = function(index, product) {
                Wholesale.product.delete({wholesaleProduct: product.id});
                $scope.products.splice(index, 1);
            };

            // add user
            $scope.addUser = function() {
                // console.log($scope.transaction.id);
                var wholesale_product = new Wholesale.product();
                wholesale_product.product = $scope.product;
                wholesale_product.quantity = $scope.quantity;
                wholesale_product.price = $scope.price;
                wholesale_product.stockNumber = $scope.itemInstance.stockNumber;
                wholesale_product.transaction = $scope.one.id;
                $scope.wp = Wholesale.product.save(wholesale_product, function() {
                    $scope.pID = $scope.wp.id;
                    // console.log("id" + $scope.pID);

                    $scope.inserted = {
                        id: $scope.pID,
                        product: parseInt($scope.product),
                        stockNumber: $scope.itemInstance.stockNumber,
                        unitPrice: $scope.itemInstance.product,
                        price: $scope.price
                    };
                    $scope.products.push($scope.inserted);
                    // console.log($scope.products);
                });
            };
        });
    }
});