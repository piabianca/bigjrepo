angular.module('BigJApp').controller('PurchaseOrderTransactionController', function($scope, $state, PurchaseOrder, Supplier, Product, Storage, SP, branchId, BP) {
    $scope.suppliers = Supplier.query();
    $scope.prods = BP.get({bp: branchId.value});
    // console.log($scope.prods);

    $scope.add = function() {
        var orderedTransaction = new PurchaseOrder.transaction();
        orderedTransaction.supplier = $scope.supplier;
        orderedTransaction.dateOrdered = new Date(Date.parse($scope.dateOrdered));
        orderedTransaction.shipVia = $scope.shipVia;
        $scope.tran = PurchaseOrder.transaction.save(orderedTransaction, function() {
            $scope.transaction = $scope.tran;
        });
    };

    $scope.onChange = function(status) {
        console.log(status);
        console.log($scope.selected);
    };

    //date
    $scope.dateOrdered = new Date();
    $scope.dateOpen = {};
    $scope.open = function($event){
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dateOpen.opened = true;
    }

    $scope.format = 'MMMM-dd-yyyy';

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    //X EDITABLE
    // $scope.productss = Product.query();
    $scope.products = [];
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.products.length; i++){
            var product = $scope.products[i];
            total += (product.unitPrice * 1);
        }
        return total;
    }

    $scope.saveUser = function(data, id) {
        PurchaseOrder.product.update({orderedProduct: id}, data, function() {
            // console.log("SUCCESS!!");
        });
        // console.log(data);
    };

    // remove user
    $scope.removeUser = function(index, product) {
        //delete purchase_order_product
        PurchaseOrder.product.delete({orderedProduct: product.id});
        //delete NEW product in storage,, put validation
        if(product.newProduct == 1) {
            // console.log("new product");
            $scope.storage = SP.get({storage: product.product}, function() {
                Storage.delete({storage: $scope.storage.id}, function() {
                });
            });
        }
        $scope.products.splice(index, 1);
    };

    // add user
    $scope.addUser = function() {
        if($scope.selected == 'new'){
            var product = new Product();
            product.name = $scope.productName;
            product.color = $scope.productColor;
            product.size = $scope.productSize;
            product.description = $scope.productDescription;
            product.price = $scope.productPrice;
            $scope.newProduct = Product.save(product, function() {
                var ordered_product = new PurchaseOrder.product();
                ordered_product.product = $scope.newProduct.id;
                ordered_product.quantity = $scope.quantity;
                ordered_product.unit = $scope.unit;
                ordered_product.unitPrice = $scope.unitPrice;
                ordered_product.transaction = $scope.transaction.id;
                ordered_product.newProduct = 1;
                $scope.op = PurchaseOrder.product.save(ordered_product, function() {
                    var storage = new Storage();
                    storage.product = $scope.newProduct.id;
                    storage.stockNumber = $scope.stockNumber;
                    //
                    // storage.quantity = $scope.quantity;
                    storage.supplier = $scope.supplier;
   ///need branch id
                    storage.branch = branchId.value;
                    $scope.store = Storage.save(storage, function() {
                        // console.log($scope.store);
                    });
                    $scope.pID = $scope.op.id;
                    $scope.inserted = {
                        id: $scope.pID,
                        product: $scope.newProduct.id,
                        quantity: $scope.quantity,
                        unit: $scope.unit,
                        stockNumber: $scope.stockNumber,
                        color: $scope.productColor,
                        description: $scope.description,
                        unitPrice: $scope.unitPrice,
                        newProduct: 1
                    };
                    $scope.products.push($scope.inserted);
                });
            });
        }
        else {
            var ordered_product = new PurchaseOrder.product();
            ordered_product.product = $scope.storageProduct;
            ordered_product.quantity = $scope.quantity;
            ordered_product.unit = $scope.unit;
            ordered_product.unitPrice = $scope.unitPrice;
            ordered_product.transaction = $scope.transaction.id;
            $scope.op = PurchaseOrder.product.save(ordered_product, function() {

                //product in storage
                // var product = SP.get({storage: $scope.storageProduct}, function() {
                //     var newQuantity = (parseInt(product.quantity) + parseInt($scope.quantity));
                //     var newData = {quantity: newQuantity};
                //     Storage.update({storage: product.id}, newData, function() {
                //         console.log("updated")
                //     });
                // });
                $scope.pID = $scope.op.id;
                $scope.inserted = {
                    id: $scope.pID,
                    product: $scope.storageProduct,
                    quantity: $scope.quantity,
                    unit: $scope.unit,
                    stockNumber: $scope.storageProduct,
                    color: $scope.storageProduct,
                    description: $scope.description,
                    unitPrice: $scope.unitPrice,
                    newProduct: 0
                };
                $scope.products.push($scope.inserted);
            });
        }
    };
});