angular.module('BigJApp').controller('BaseController', function ($scope, $auth, $state, branchId) {

    $scope.changeBranchId = function (newId) {
        branchId.value = newId;
        $scope.branch = branchId.value;
    };

    $scope.logout = function () {
        $auth.logout();
        $state.go('signin');
    };

    $scope.isAuthenticated = function () {
        return $auth.isAuthenticated();
    };
});
