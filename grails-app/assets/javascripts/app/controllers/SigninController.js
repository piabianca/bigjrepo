angular.module('BigJApp').controller('SigninController', function ($scope, $auth, $state) {
    $scope.login = function () {
        var user = {
            username: $scope.username,
            password: $scope.password
        };

        $auth.login(user)
            .then(function (response) {
                $state.go('products');
            });
    };
});