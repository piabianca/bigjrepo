angular.module('BigJApp').controller('HomeController', function ($scope, $state, User) {
    $scope.nonUsers = User.GetNonUsers.query();

    $scope.changeStatus = function (user) {
        User.ChangeStatus.get({id: user});
        setTimeout(function () {
            $scope.nonUsers = User.GetNonUsers.query();
            $state.reload();
        }, 1000);
    };

    $scope.accessibleBranches = User.GetAccessibleBranches.query();
    $scope.currentUser=User.UserLoggedIn.get();
    $scope.isAdmin = function () {
        if ($scope.accessibleBranches.length == 3) {
            return true;
        }
        return false;
    };
});
