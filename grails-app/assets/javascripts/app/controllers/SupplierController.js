angular.module('BigJApp').controller('SupplierController', function($scope, $state, Supplier) {
  $scope.suppliers = Supplier.query();
  $scope.displayedCollection = [].concat($scope.suppliers);

    $scope.add = function() {

        var supplier = new Supplier();
        supplier.name = $scope.supplierName;
        supplier.contactNumber = $scope.supplierContactNumber;
        supplier.address = $scope.supplierAddress;
        Supplier.save(supplier, function() {
        	$state.go('suppliers');
        });
    };
});
