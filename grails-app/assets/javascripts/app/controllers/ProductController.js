angular.module('BigJApp').controller('ProductController', function ($scope, Product, Upload, SubCategory, $state, Category) {

    $scope.Categories = Category.query();
    $scope.subCategories = SubCategory.query();
    $scope.selectedCategory = null;
    $scope.newCategory = null;

    $scope.submit = function () {
        if ($scope.file) {
            $scope.upload($scope.file);
        }
    };

    $scope.setCategory = function (category) {
        $scope.selectedCategory = category;
    };

    // upload on file select or drop
    $scope.upload = function (file) {
        Upload.upload({
            url: '/BigJRepository/upload/',
            data: {file: file},
            file: file,
            fileFormDataName: "myFile"
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };
    $scope.editUpload = function (file, id) {
        Upload.upload({
            url: '/BigJRepository/editUpload/' + id,
            data: {file: file},
            file: file,
            fileFormDataName: "myFile"
        });
    };

    $scope.checkPicture = function (row) {
        // console.log(row.photo_extension);
        if (row.photo_extension == null) {
            $scope.location = "default.jpg";
        } else {
            $scope.location = row.id + "." + row.photo_extension;
        }
        return true;
    };

    $scope.add = function () {
        var product = new Product();
        product.name = $scope.formInfo.itemName;
        product.description = $scope.formInfo.description;
        product.stock_number = $scope.formInfo.stockNumber;
        product.color = $scope.formInfo.color;
        product.size = $scope.formInfo.size;
        product.subCategory = $scope.formInfo.subCategory;
        // product.quantity = $scope.formInfo.quantity;
        product.price = $scope.formInfo.price;
        Product.save(product, function () {
            if ($scope.file) {
                $scope.upload($scope.file);
            }
            setTimeout(function () {
                $scope.rowCollection = Product.query();
            }, 1000);
            $state.go('products', {});
        });
        $scope.clearInput();
    };

    $scope.update = function (productID) {
        var product = Product.get({product: productID});
        product.name = $scope.oneProduct.name;
        product.description = $scope.oneProduct.description;
        product.stock_number = $scope.oneProduct.stock_number;
        product.color = $scope.oneProduct.color;
        product.size = $scope.oneProduct.size;
        // product.quantity = $scope.oneProduct.quantity;
        product.price = $scope.oneProduct.price;
        product.subCategory = $scope.oneProduct.subCategory;
        Product.update({product: productID}, product, function () {
            if ($scope.editFile) {
                $scope.editUpload($scope.editFile, productID);
            }
            setTimeout(function () {
                $scope.rowCollection = Product.query();
            }, 1000);
        });
    }

    $scope.name = "JIGS";
    $scope.formInfo = {};
    $scope.edit = function (productID) {
        $scope.oneProduct = Product.get({product: productID});
    }

    $scope.showModal = function (id) {
        $scope.oneProduct = Product.get({product: id});
    };

    $scope.clearInput = function () {
        $scope.formInfo.itemName = null;
        $scope.formInfo.description = null;
        $scope.formInfo.stockNumber = null;
        $scope.formInfo.color = null;
        $scope.formInfo.size = null;
        $scope.formInfo.subCategory = null;
        // $scope.formInfo.quantity =null;
        $scope.formInfo.price = null;
    };


    //////////////////////////////////////////////////////////////
    $scope.rowCollection = Product.query();
    $scope.searchBy = "";

    $scope.displayedCollection = [].concat($scope.rowCollection);

    //remove to the real data holder
    $scope.removeItem = function removeItem(row) {
        var index = $scope.rowCollection.indexOf(row);
        if (index !== -1) {
            $scope.rowCollection.splice(index, 1);
        }
        Product.delete({product: row.id});
    }

    $scope.isUpdated = function (row) {
        var today = new Date();
        if (!row.last) {
            return false;
        }
        var productUpdated = new Date(row.last);
        if (today.getFullYear() == productUpdated.getFullYear() && today.getMonth() == productUpdated.getMonth()) {
            if (productUpdated.getDate() == today.getDate() || productUpdated.getDate() < (today.getDate() - 10)) {
                return true;
            }
        }
        return false;
    }

    $scope.isPriceChanged = function (row) {
        if (row.hasIncreasedPrice != null) {
            return true
        }
        return false
    }

    $scope.getSubCategory = function (id) {
        return $scope.subCategories[id-1];
    };
});
