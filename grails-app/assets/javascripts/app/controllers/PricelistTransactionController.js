angular.module('BigJApp').controller('PricelistTransactionController', function ($scope, $state, BP, branchId, Wholesale, Client, Product, Storage, SP) {
    $scope.clients = Client.query();
    $scope.add = function () {
        var wholesaleTransaction = new Wholesale.transaction();
        wholesaleTransaction.client = $scope.client;
        wholesaleTransaction.status = $scope.status;
        $scope.tran = Wholesale.transaction.save(wholesaleTransaction, function () {
            $scope.transaction = $scope.tran;
        });
    };

    //X EDITABLE
    // $scope.productss = Product.query();
    $scope.productss = BP.get({bp: branchId.value});
    $scope.products = [];
    // $scope.getTotal = function () {
    //     var total = 0;
    //     for (var i = 0; i < $scope.products.length; i++) {
    //         var product = $scope.products[i];
    //         total += (product.price * product.quantity);
    //     }
    //     return total;
    // }

    $scope.selectedItemChanged = function (product) {
        $scope.item = SP.get({storage: product}, function () {
            $scope.itemInstance = $scope.item;
        });
    }

    $scope.groups = [];
    $scope.loadGroups = function () {
        return $scope.groups.length ? null : Product.query(function (data) {
            $scope.groups = data;
        });
    };

    $scope.showGroup = function (user) {
        if (user.group && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, {id: user.group});
            return selected.length ? selected[0].text : 'Not set';
        } else {
            return user.product || 'Not set';
        }
    };

    $scope.showUnitPrice = function() {

    };

    $scope.saveUser = function (data, id) {
        // var originalQuantity = $scope.itemInstance.quantity;
        // var newQuantity = originalQuantity - parseInt(data.quantity);
        // var newData = {quantity: newQuantity};
        // Storage.update({storage: $scope.itemInstance.id}, newData);
        Wholesale.product.update({wholesaleProduct: id}, data, function () {
            console.log(data);
        });
    };

    // remove user
    $scope.removeUser = function (index, product) {
        // var originalQuantity = $scope.itemInstance.quantity;
        // var newData = {quantity: originalQuantity};
        // Storage.update({storage: $scope.itemInstance.id}, newData);
        Wholesale.product.delete({wholesaleProduct: product.id});
        $scope.products.splice(index, 1);
    };

    // add user
    $scope.addUser = function () {
        // console.log($scope.itemInstance);
        var wholesale_product = new Wholesale.product();
        wholesale_product.product = $scope.product;
        wholesale_product.price = $scope.price;
        wholesale_product.stockNumber = $scope.itemInstance.stockNumber;
        wholesale_product.transaction = $scope.transaction.id;
        $scope.wp = Wholesale.product.save(wholesale_product, function () {
            $scope.pID = $scope.wp.id;
            $scope.inserted = {
                id: $scope.pID,
                product: parseInt($scope.product),
                stockNumber: $scope.itemInstance.stockNumber,
                unitPrice: $scope.itemInstance.product,
                price: $scope.price
            };
            $scope.products.push($scope.inserted);
            // var storage = SP.get({storage: $scope.product}, function () {
            //     var quantity = storage.quantity;
            //     var newQuantity = quantity - parseInt($scope.quantity);
            //     var data = {quantity: newQuantity};
            //     Storage.update({storage: storage.id}, data, function () {
            //         console.log("update successful");
            //     });
            // });
            // console.log($scope.products);
        });
    };
});
