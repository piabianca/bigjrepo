angular.module('BigJApp').controller('ClientController', function($scope, $state, Client) {
    $scope.clients = Client.query();
    $scope.displayedCollection = [].concat($scope.clients);

    $scope.add = function() {
        var client = new Client();
        client.name = $scope.clientName;
        client.contactNumber = $scope.clientContactNumber;
        client.address = $scope.clientAddress;
        Client.save(client, function() {
        	$state.go('clients');
        });
    };
});
