angular.module('BigJApp').controller('StorageController', function ($scope, $state, Storage, Product, User, Client, branchId) {
    $scope.add = function () {
        var storage = new Storage();
        //product, quantity, client
        Storage.save(storage);
    };
    $scope.products = Product.query();
    
    $scope.rowCollection = Storage.query();
    $scope.displayedCollection = [].concat($scope.rowCollection);

    $scope.getProduct = function (productID) {
        return $scope.products[productID - 1];
    }

    $scope.accessibleBranches = User.GetAccessibleBranches.query();

    $scope.accessAllowed = function () {
        var i;
        for (i = 0; i < $scope.accessibleBranches.length; i++) {
            if (branchId.value == $scope.accessibleBranches[i].id) {
                return true;
            }
        }
        return false;
    }
});
