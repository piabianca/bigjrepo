import bigjrepository.Product
import bigjrepository.Category
import bigjrepository.SubCategory
import bigjrepository.Type
import bigjrepository.Client
import bigjrepository.Supplier
import bigjrepository.Storage
import bigjrepository.WholesaleProduct
import bigjrepository.WholesaleTransaction
import bigjrepository.PurchaseOrderProduct
import bigjrepository.PurchaseOrderTransaction
import bigjrepository.Branch
import auth.*

class BootStrap {

    def init = { servletContext ->
        def cat1 = new Category(name: "kitchenware").save(flush: true)
        def cat2 = new Category(name: "school").save(flush: true)

        def sub1 = new SubCategory(name: "spoon", category: cat1).save(flush: true)
        def sub2 = new SubCategory(name: "uniform", category: cat2).save(flush: true)
        def sub3 = new SubCategory(name: "fork", category: cat1).save(flush: true)

        def type1 = new Type(name: "wooden").save(flush: true)
        def type2 = new Type(name: "steel").save(flush: true)

        def prod1 = new Product(name: "shoe", color: "blue", size: "big", description: "blue shoe",
                stock_number: "#0009*", price: 50.0, subCategory: sub1).save(flush: true)

        def prod2 = new Product(name: "shoess", color: "blue", size: "big", description: "blue shoe",
                stock_number: "#0009*", price: 50.0, subCategory: sub2).save(flush: true)

        //wholesale
        def client1 = new Client(name: "Jepoy", contactNumber: "12345", address: "CPU").save(flush: true)

        def wholesale_trans1 = new WholesaleTransaction(client: client1).save(flush: true)

        def whole_prod1 = new WholesaleProduct(product: prod1, price: 100.0, transaction: wholesale_trans1).save(flush: true)
        def whole_prod2 = new WholesaleProduct(product: prod2, price: 150.0, transaction: wholesale_trans1).save(flush: true)

        //branch
        def branch1 = new Branch(name: "Aldeguer").save(flush: true)
        def branch2 = new Branch(name: "Hoskyn").save(flush: true)
        def branch3 = new Branch(name: "Tanza").save(flush: true)

        
        //order
        def supplier1 = new Supplier(name: "Jigs", contactNumber: "54321", address: "CPU engg").save(flush: true)

        def ordered_trans1 = new PurchaseOrderTransaction(supplier: supplier1, shipVia: "boat", dateOrdered: new Date(), branch: branch1).save(flush: true)

        def ordered_prod1 = new PurchaseOrderProduct(product:
                new Product(name: "lotion", color: "white", size: "S", description: "white lotion", stock_number: "#000119*", price: 10.0).save(flush: true),
                quantity: 5000, unit: "big box", unitQuantity: 2, unitPrice: 1000000, transaction: ordered_trans1).save(flush: true)

        def ordered_prod2 = new PurchaseOrderProduct(product:
                new Product(name: "laptop", color: "white", size: "M", description: "white laptop", stock_number: "#01100119*", price: 25000.0).save(flush: true),
                quantity: 10, unit: "box", unitQuantity: 5, unitPrice: 250000, transaction: ordered_trans1).save(flush: true)


        //storage
        def store1 = new Storage(product: prod1, stockNumber: "123-32-12", supplier: supplier1, quantity: 100, branch: branch1).save(flush: true)
        def store2 = new Storage(product: prod2, stockNumber: "123-32-12", supplier: supplier1, quantity: 10, branch: branch1).save(flush: true)
        def store3 = new Storage(product: 3, stockNumber: "123-32-12", supplier: supplier1, quantity: 10, branch: branch1).save(flush: true)
        def store4 = new Storage(product: 3, stockNumber: "123-32-12", supplier: supplier1, quantity: 10, branch: branch2).save(flush: true)
        def store5 = new Storage(product: prod1, stockNumber: "123-32-12", supplier: supplier1, quantity: 10, branch: branch3).save(flush: true)
        def store6 = new Storage(product: 4, stockNumber: "123-32-12", supplier: supplier1, quantity: 10, branch: branch3).save(flush: true)

        def nonUser = new Role(authority: 'NON_USER').save(flush: true)
        def adminUser = new Role(authority: 'ADMIN_USER', accessible_braches: [branch1, branch2, branch3]).save(flush: true)
        def aldeguerUser = new Role(authority: 'ALDEGUER_USER', accessible_braches: branch1).save(flush: true)
        def hoskynUser = new Role(authority: 'HOSKYN_USER', accessible_braches: branch2).save(flush: true)
        def tanzaUser = new Role(authority: 'TANZA_USER', accessible_braches: branch3).save(flush: true)

//        adminUser.addToAccesable_braches(branch1).save()
//        adminUser.addToAccesable_braches(branch2).save()
//        adminUser.addToAccesable_braches(branch3).save()
//
//        aldeguerUser.addToAccesable_braches(branch1).save()
//        hoskynUser.addToAccesable_braches(branch2).save()
//        tanzaUser.addToAccesable_braches(branch3).save()

        def admin = new User(username: "admin", password: "1234").save(failOnError: true)
        def aUser = new User(username: "aldeguer", password: "1234").save(failOnError: true)
        def hUser = new User(username: "hoskyn", password: "1234").save(failOnError: true)
        def nUser = new User(username: "aldeguer2", password: "1234", requested_role: 3).save(failOnError: true)

        def adminRole = Role.findByAuthority('ADMIN_USER')
        def aldeguerRole = Role.findByAuthority('ALDEGUER_USER')
        def hoskynRole = Role.findByAuthority('HOSKYN_USER')

        UserRole.create(admin, adminRole, true)
        UserRole.create(aUser, aldeguerRole, true)
        UserRole.create(hUser, hoskynRole, true)
        UserRole.create(nUser, nonUser, true)

    }
    def destroy = {
    }
}
