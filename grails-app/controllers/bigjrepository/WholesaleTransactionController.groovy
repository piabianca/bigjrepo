package bigjrepository



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
@Transactional(readOnly = true)
class WholesaleTransactionController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond WholesaleTransaction.list(params), [status: OK]
    }

    def show(WholesaleTransaction wholesaleTransactionInstance) {
        respond wholesaleTransactionInstance
    }

    @Transactional
    def save(WholesaleTransaction wholesaleTransactionInstance) {
        if (wholesaleTransactionInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleTransactionInstance.validate()
        if (wholesaleTransactionInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        wholesaleTransactionInstance.save flush:true
        respond wholesaleTransactionInstance, [status: CREATED]
    }

    @Transactional
    def update(WholesaleTransaction wholesaleTransactionInstance) {
        if (wholesaleTransactionInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleTransactionInstance.validate()
        if (wholesaleTransactionInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        wholesaleTransactionInstance.save flush:true
        respond wholesaleTransactionInstance, [status: OK]
    }

    @Transactional
    def delete(WholesaleTransaction wholesaleTransactionInstance) {

        if (wholesaleTransactionInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleTransactionInstance.delete flush:true
        render status: NO_CONTENT
    }
}
