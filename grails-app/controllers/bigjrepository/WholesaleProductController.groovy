package bigjrepository



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
@Transactional(readOnly = true)
class WholesaleProductController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond WholesaleProduct.list(params), [status: OK]
    }

    def products() {
        def results = WholesaleProduct.findAll("from WholesaleProduct where transaction =" + params.id)
        respond results, [status: OK]
    }

    @Transactional
    def save(WholesaleProduct wholesaleProductInstance) {
        if (wholesaleProductInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleProductInstance.validate()
        if (wholesaleProductInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        wholesaleProductInstance.save flush:true
        respond wholesaleProductInstance, [status: CREATED]
    }

    @Transactional
    def update(WholesaleProduct wholesaleProductInstance) {
        if (wholesaleProductInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleProductInstance.validate()
        if (wholesaleProductInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        wholesaleProductInstance.save flush:true
        respond wholesaleProductInstance, [status: OK]
    }

    @Transactional
    def delete(WholesaleProduct wholesaleProductInstance) {

        if (wholesaleProductInstance == null) {
            render status: NOT_FOUND
            return
        }

        wholesaleProductInstance.delete flush:true
        render status: NO_CONTENT
    }
}
